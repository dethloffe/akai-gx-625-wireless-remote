import machine
import utime
from ir_rx.print_error import print_error
from ir_rx.nec import NEC_8

# Declare pin locations on pi pico
led_pin = machine.Pin(25, machine.Pin.OUT)			# cable
pin_ir = machine.Pin(2, machine.Pin.IN)				# brown cable
play_pin = machine.Pin(9, machine.Pin.OUT)			# blue cable
pause_pin = machine.Pin(7, machine.Pin.OUT)			# yellow cable
stop_pin = machine.Pin(5, machine.Pin.OUT)			# brown cable
fast_forward_pin = machine.Pin(8, machine.Pin.OUT)	# green cable
rewind_pin = machine.Pin(4, machine.Pin.OUT)		# black cable
record_pin = machine.Pin(6, machine.Pin.OUT)		# orange cable

# Constants for pin on/off values
on = 0
off = 1

# Turn off all pins when booting up
play_pin.value(off)
pause_pin.value(off)
stop_pin.value(off)
fast_forward_pin.value(off)
rewind_pin.value(off)
record_pin.value(off)

def decodeKeyValue(data):
    if data == 0x16:
        # The 0 button
        stop_pin.value(on)
        utime.sleep(.5)
        stop_pin.value(off)
        return "STOP"
    if data == 0x07:
        # The 1 button
        print("Pausing")
        pause_pin.value(on)
        utime.sleep(.5)
        pause_pin.value(off)
        return "1"
    if data == 0x44:
        # The << button
        rewind_pin.value(on)
        utime.sleep(.5)
        rewind_pin.value(off)
        return "REWIND"
    if data == 0x40:
        # The >> Button
        fast_forward_pin.value(on)
        utime.sleep(.5)
        fast_forward_pin.value(off)
        return "FORWARD"
    if data == 0x43:
        # The Play/Pause button
        play_pin.value(on)
        utime.sleep(.5)
        play_pin.value(off)
        return "PLAY/PAUSE"
    return "ERROR"

def callback(data, addr, ctrl):
    if data < 0: # NEC protocol sends repeat codes.
        pass
    else:
        print(data)
        print(decodeKeyValue(data))

ir = NEC_8(pin_ir, callback)    # Instantiate receiver
ir.error_function(print_error)  # Show debug information