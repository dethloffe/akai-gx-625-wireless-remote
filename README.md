# Akai GX-625 Wireless Remote

## Description
This project allows you to access the playback features on your Akai reel to reel machine wirelessly and affordably.

## Installation
You will need a Raspberry Pi Pico and a IR reciever / transmitter. I would refer you to the setup.png image in this repository to see how I set up the Pico and connected it to the tape machine. You can add the main.py file to your Pi Pico and it will run on start up, you may need to adjust the GPIO pin numbers in the main.py file depending on how you hook up your device.

## Project status
I no longer have my tape machine since I didn't use it as much as I do my turntable so I stopped any improvements on this project. If life provides the opportunity, I would like to get another machine and create a phone app to control the device. I would be happy to asnwer any questions you have or assist with your setup if you would like to recreate this project. I can provide links to where I got the equipment from.

## Video
https://youtube.com/shorts/Ga2WEyj1TH0?si=2g0dcbfz2LYv5x6E